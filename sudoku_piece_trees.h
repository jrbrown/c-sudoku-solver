// 15/09/2020
// Jason Brown
// Sudoku solver sudoku_piece_tree.h


#ifndef SUDOKU_SOLVER_SUDOKU_PIECE_TREES_H
#define SUDOKU_SOLVER_SUDOKU_PIECE_TREES_H

#endif //SUDOKU_SOLVER_SUDOKU_PIECE_TREES_H


// Data structure to store trees of solutions, Could be used for boxes, rows or columns
typedef struct piece_tree_node {
    int values[9];  // Stores values of the piece, 0 means unknown
    int value;  // Equivalent to values[pieceTree->depth - 1]
    int depth;  // How far down tree we are. E.g. 0 is no known values, 9 is all known values
    struct piece_tree_node * branches[10];  // For a pointer in this data structure, its position corresponds to the cell value of the piece further down the tree
    // For example if we have a piece_tree that has the next value and it's value 3, we put the pointer in branches[3]
    // branches[0] should be NULL, could make branches only size 9 but this way the indexing is more intuitive
    struct piece_tree_node * parent;  // Parent piece_tree_node
} piece_tree_t;


// Functions manipulating piece_trees
piece_tree_t * create_new_piece_tree_root();
void add_value_to_piece_tree(piece_tree_t * pieceTree, int value);  // When we add a new piece we use the root and the chain of values. 0 for the unknowns
void add_values_to_piece_tree_breadth(piece_tree_t * pieceTree, int values[9], void (*add_value_function)(piece_tree_t*, int));
void add_values_to_piece_tree_depth(piece_tree_t * pieceTree, int values[9], void (*add_value_function)(piece_tree_t*, int));
void add_values_to_all_branches_at_depth(piece_tree_t * pieceTree, int values[9], int depth);
piece_tree_t * generate_tree_from_possible_values_array(int possible_values[9][9]);
void delete_branch(piece_tree_t * pieceTree);
piece_tree_t * return_next_filled_piece(piece_tree_t * pieceTree);
void compare_and_delete_non_compatible_trees(piece_tree_t * tree1, piece_tree_t * tree2, int tree1_positions[3], int tree2_positions[3]);
piece_tree_t * traverse_tree(piece_tree_t * pieceTree); // Breadth first, if at end of row goes back to first row element and descends
piece_tree_t * descend_tree(piece_tree_t * pieceTree); // Depth first, if at end of tree goes back to parent element and descends next child

// Debug functions, not all listed
void print_piece(piece_tree_t * pieceTree);
void print_piece_tree_depth_first(piece_tree_t * pieceTree);
void print_piece_tree_breadth_first(piece_tree_t * pieceTree);
void print_filled_pieces(piece_tree_t * pieceTree);
void piece_tree_testing();
