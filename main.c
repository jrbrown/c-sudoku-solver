// 15/09/2020
// Jason Brown
// Sudoku solver main.c


#include "sudoku_piece_trees.h"

#include <stdio.h>
#include <windows.h>


#define DELAY 100


void print_puzzle(int (*puzzle)[9][9]){
    for(int i=0; i<9; i++){
        for(int j=0; j<9; j++){
            printf("%i ", (*puzzle)[i][j]);
        }
        printf("\n");
    }
    printf("\n");
}

void update_value_matrix(int (*puzzle)[9][9], int (*possible_value_matrix)[9][9][10], int *num_cells_solved){
    *num_cells_solved = 0;

    // Set all values in possible value matrix to 1
    for(int i=0; i<9; i++){
        for(int j=0; j<9; j++){
            for(int k=1; k<10; k++){
                (*possible_value_matrix)[i][j][k] = 1;
            }
        }
    }

    // Iterate through cells and update value matrix based on values in puzzle
    for(int i=0; i<9; i++){
        for(int j=0; j<9; j++){
            int value = (*puzzle)[i][j];

            // Check if a value is in this cell
            if(value != 0){

                // Note that a cell is solved
                (*num_cells_solved)++;

                // Update possible values in this row
                for(int k=0; k<9; k++){
                    (*possible_value_matrix)[i][k][value] = 0;
                }

                // Update possible values in this col
                for(int k=0; k<9; k++){
                    (*possible_value_matrix)[k][j][value] = 0;
                }

                // Update possible values in this box
                for(int k=0; k<9; k++){
                    int box_i = i/3;
                    int box_j = j/3;
                    int box_row = k/3;
                    int box_col = k - (3 * box_row);
                    int i_coord = (3 * box_i) + box_row;
                    int j_coord = (3 * box_j) + box_col;
                    (*possible_value_matrix)[i_coord][j_coord][value] = 0;
                }

                // Update this cells possible value matrix
                for(int k=1; k<10; k++){
                    (*possible_value_matrix)[i][j][k] = 0;
                }
                (*possible_value_matrix)[i][j][value] = 1;
            }
        }
    }
}

void advanced_solve(int (*puzzle)[9][9], int (*possible_value_matrix)[9][9][10], int *num_cells_solved){

    printf("\nRough method failed, performing advanced solve\n");

    // Perform normal value update
    update_value_matrix(puzzle, possible_value_matrix, num_cells_solved);

    // We'll generate the set of possibilities for each box, row, column and then cross check them and remove any which
    // are incompatible
    // We can either re-iterate this process until we solve the puzzle or we can decompose the leftover solutions for
    // each individual piece and rewrite the possible value matrix to be more accurate

    // Storage for our generated trees
    piece_tree_t * box_trees[9] = {NULL};
    piece_tree_t * row_trees[9] = {NULL};
    piece_tree_t * col_trees[9] = {NULL};

    // Generate our trees
    // Boxes
    for(int box=0; box<9; box++) {
        int box_possible_values[9][9] = {{0}};  // See piece_tree_testing in sudoku_piece_trees.c for what this should look like

        // Using generate_tree_from_possible_values_array we can easily create our tree, but we need to get extract the
        // data from possible_value_matrix first.
        for(int box_element=0; box_element<9; box_element++){

            // Figure out what our coords are given the status of the box iterators
            int i = (3 * (box / 3)) + (box_element / 3);
            int j = (3 * (box % 3)) + (box_element % 3);

            // Iterate through possible values matrix and add our values to box_possible_values
            for(int k=1; k<10; k++){
                if((*possible_value_matrix)[i][j][k] == 1){
                    box_possible_values[box_element][(k-1)] = k;
                }
            }
        }

        // Now we have our values we can generate a tree for this box
        box_trees[box] = generate_tree_from_possible_values_array(box_possible_values);
    }

    // Rows
    for(int i=0; i<9; i++) {
        int row_possible_values[9][9] = {{0}};  // See piece_tree_testing in sudoku_piece_trees.c for what this should look like

        // Using generate_tree_from_possible_values_array we can easily create our tree, but we need to get extract the
        // data from possible_value_matrix first.
        for(int j=0; j<9; j++){

            // Iterate through possible values matrix and add our values to row_possible_values
            for(int k=1; k<10; k++){
                if((*possible_value_matrix)[i][j][k] == 1){
                    row_possible_values[j][(k-1)] = k;
                }
            }
        }

        // Now we have our values we can generate a tree for this row
        row_trees[i] = generate_tree_from_possible_values_array(row_possible_values);
    }

    // Columns
    for(int j=0; j<9; j++) {
        int col_possible_values[9][9] = {{0}};  // See piece_tree_testing in sudoku_piece_trees.c for what this should look like

        // Using generate_tree_from_possible_values_array we can easily create our tree, but we need to get extract the
        // data from possible_value_matrix first.
        for(int i=0; i<9; i++){

            // Iterate through possible values matrix and add our values to col_possible_values
            for(int k=1; k<10; k++){
                if((*possible_value_matrix)[i][j][k] == 1){
                    col_possible_values[i][(k-1)] = k;
                }
            }
        }

        // Now we have our values we can generate a tree for this col
        col_trees[j] = generate_tree_from_possible_values_array(col_possible_values);
    }

    // Eliminate incompatible solutions, iterate until solved
    int iterations = 0;

    while(TRUE){

        iterations++;

        // Cross check box with row and col
        for (int box = 0; box < 9; box++) {

            // Check box against rows
            for (int i = 0; i < 3; i++) {

                // Positions to check in our box
                int box_positions[3] = {
                        (i * 3),
                        ((i * 3) + 1),
                        ((i * 3) + 2)
                };

                // Positions to check in our row
                int row_positions[3] = {
                        ((box % 3) * 3),
                        (((box % 3) * 3) + 1),
                        (((box % 3) * 3) + 2)
                };

                // Calculate row to check
                int row = ((box / 3) * 3) + i;

                // Perform check
                compare_and_delete_non_compatible_trees(box_trees[box], row_trees[row], box_positions, row_positions);
            }

            // Check box against columns
            for (int j = 0; j < 3; j++) {

                // Positions to check in our box
                int box_positions[3] = {
                        (j % 3),
                        ((j % 3) + 3),
                        ((j % 3) + 6)
                };

                // Positions to check in our row
                int col_positions[3] = {
                        ((box / 3) * 3),
                        (((box / 3) * 3) + 1),
                        (((box / 3) * 3) + 2)
                };

                // Calculate row to check
                int col = ((box % 3) * 3) + j;

                // Perform check
                compare_and_delete_non_compatible_trees(box_trees[box], col_trees[col], box_positions, col_positions);
            }
        }

        // Cross check rows and columns
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                int row_positions[3] = {j, j, j};
                int col_positions[3] = {i, i, i};
                compare_and_delete_non_compatible_trees(row_trees[i], col_trees[j], row_positions, col_positions);
            }
        }

        // Check if solved
        *num_cells_solved = 0;

        for(int i=0; i<9; i++){

            // Check if only 1 solution for row exists
            if(return_next_filled_piece(row_trees[i]) == return_next_filled_piece(return_next_filled_piece(row_trees[i]))){

                *num_cells_solved += 9;

                // Update that rows values
                for(int j=0; j<9; j++){
                    (*puzzle)[i][j] = return_next_filled_piece(row_trees[i])->values[j];
                }
            }
        }

        if(*num_cells_solved == 81){
            break;
        }
    }
    printf("Advanced solve completed in %i iterations\n", iterations);
}

int check_progress_and_update_value_matrix(int (*puzzle)[9][9], int (*possible_value_matrix)[9][9][10]){
    // Used to test when we've solved the puzzle
    int num_cells_solved = 0;

    // Used to track puzzle progress
    static int old_puzzle[9][9] = {{0}};
    static int times_not_made_any_progress = 0;
    int progress_made = 0;

    // Check if we made progress solving the puzzle on the last iteration, we increment progress_made for each difference
    for(int i=0; i<9; i++){
        for(int j=0; j<9; j++){
            progress_made += (old_puzzle[i][j] != (*puzzle)[i][j]);
        }
    }

    // Update old_puzzle
    for(int i=0; i<9; i++){
        for(int j=0; j<9; j++){
            old_puzzle[i][j] = (*puzzle)[i][j];
        }
    }

    if(!progress_made){
        times_not_made_any_progress++;
    }

    if(times_not_made_any_progress < 2){
        update_value_matrix(puzzle, possible_value_matrix, &num_cells_solved);
    } else {
        advanced_solve(puzzle, possible_value_matrix, &num_cells_solved);
        return 2;
    }

    return num_cells_solved == 81;
}

void solve1(int (*puzzle)[9][9], int verbose){

    int iterations = 0;
    int solved_via_advanced_solve = FALSE;

    while(TRUE){

        iterations++;

        // Print puzzle
        if(verbose){
            print_puzzle(puzzle);
        }

        // Initialise possible values matrix
        int possible_value_matrix[9][9][10] = {{{0}}};

        // Update possible values matrix
        int solved_flag = check_progress_and_update_value_matrix(puzzle, &possible_value_matrix);
        if(solved_flag == 1){  // Solved normally
            break;
        } else if(solved_flag == 2){
            solved_via_advanced_solve = TRUE;
            break;
        }

        // Put in cells that only have 1 possible value
        for(int i=0; i<9; i++){
            for(int j=0; j<9; j++){

                // Check if already filled
                if((*puzzle)[i][j] == 0){

                    // To keep track for when there's more than 1 value to assign
                    int times_assigned = 0;

                    // Assign value to cell
                    for(int k=1; k<10; k++){
                        if(possible_value_matrix[i][j][k]){
                            times_assigned++;
                            (*puzzle)[i][j] = k;
                        }
                    }

                    // If we assigned more than once we need to reset back to 0 as there's more than 1 possible value
                    if(times_assigned > 1){
                        (*puzzle)[i][j] = 0;
                    }
                }
            }
        }

        // Update possible values matrix
        solved_flag = check_progress_and_update_value_matrix(puzzle, &possible_value_matrix);
        if(solved_flag == 1){  // Solved normally
            break;
        } else if(solved_flag == 2){
            solved_via_advanced_solve = TRUE;
            break;
        }

        // Check rows, columns and boxes for each value to see if there's only one possible location
        // Iterate for each value 1-9
        for(int k=1; k<10; k++){

            // Rows
            for(int i=0; i<9; i++) {

                // Check how many times this value is present in the row in possible value matrix
                // Records the position of the last one found in case it's the only one
                int num_values_found = 0;
                int last_value_i = 0;
                int last_value_j = 0;

                for(int j=0; j<9; j++){
                    if(possible_value_matrix[i][j][k]){
                        num_values_found++;
                        last_value_i = i;
                        last_value_j = j;
                    }
                }

                // If we found a single value we should update puzzle
                if(num_values_found == 1){
                    (*puzzle)[last_value_i][last_value_j] = k;
                }
            }

            // Columns
            for(int j=0; j<9; j++) {

                // Check how many times this value is present in the column in possible value matrix
                // Records the position of the last one found in case it's the only one
                int num_values_found = 0;
                int last_value_i = 0;
                int last_value_j = 0;

                for(int i=0; i<9; i++){
                    if(possible_value_matrix[i][j][k]){
                        num_values_found++;
                        last_value_i = i;
                        last_value_j = j;
                    }
                }

                // If we found a single value we should update puzzle
                if(num_values_found == 1){
                    (*puzzle)[last_value_i][last_value_j] = k;
                }
            }

            // Boxes
            for(int box=0; box<9; box++) {

                // Check how many times this value is present in the box in possible value matrix
                // Records the position of the last one found in case it's the only one
                int num_values_found = 0;
                int last_value_i = 0;
                int last_value_j = 0;

                for(int box_element=0; box_element<9; box_element++){

                    // Figure out what our coords are given the status of the box iterators
                    int i = (3 * (box / 3)) + (box_element / 3);
                    int j = (3 * (box % 3)) + (box_element % 3);

                    if(possible_value_matrix[i][j][k]){
                        num_values_found++;
                        last_value_i = i;
                        last_value_j = j;
                    }
                }

                // If we found a single value we should update puzzle
                if(num_values_found == 1){
                    (*puzzle)[last_value_i][last_value_j] = k;
                }
            }
        }

        Sleep(DELAY);
    }

    if(solved_via_advanced_solve == FALSE){
        printf("\nSolved in %i iterations.\n", iterations);
    }
    printf("\n");
    print_puzzle(puzzle);
}

int main() {
    // Solved
    int easy_puzzle1[9][9] = {
            {9, 7, 0,   0, 0, 0,   6, 0, 0},
            {2 ,0, 1,   5, 0, 9,   0, 3, 4},
            {8, 3, 0,   0, 4, 0,   0, 1, 0},

            {0, 0, 0,   4, 0, 2,   0, 0, 0},
            {7, 0, 6,   0, 5, 0,   0, 0, 2},
            {0, 5, 2,   0, 3, 8,   0, 0, 0},

            {5, 0, 0,   8, 1, 7,   0, 0, 6},
            {6, 2, 0,   3, 9, 4,   0, 5, 1},
            {0, 0, 0,   0, 6, 0,   4, 0, 3}
    };

    // Solved
    int hard_puzzle1[9][9] = {
            {0, 0, 1,   0, 7, 0,   0, 0, 9},
            {4 ,0, 0,   8, 0, 0,   7, 0, 2},
            {2, 8, 0,   0, 0, 0,   0, 1, 0},

            {0, 4, 0,   0, 5, 0,   2, 0, 8},
            {0, 0, 0,   2, 8, 0,   0, 9, 0},
            {0, 0, 0,   0, 0, 0,   0, 0, 0},

            {0, 9, 0,   0, 0, 0,   0, 7, 0},
            {0, 0, 0,   0, 0, 3,   0, 0, 4},
            {7, 0, 0,   0, 2, 0,   1, 6, 0}
    };

    // Unsolved
    int expert_puzzle1[9][9] = {
            {0, 2, 0,   0, 0, 0,   0, 0, 3},
            {6 ,0, 0,   0, 3, 1,   0, 0, 0},
            {5, 0, 0,   0, 0, 0,   0, 8, 4},

            {3, 7, 0,   0, 0, 0,   5, 0, 1},
            {0, 0, 0,   0, 6, 0,   0, 0, 9},
            {0, 0, 0,   4, 0, 0,   0, 0, 0},

            {0, 0, 0,   0, 0, 7,   8, 0, 0},
            {2, 0, 0,   0, 9, 0,   0, 4, 0},
            {0, 5, 0,   2, 0, 0,   1, 0, 0}
    };

    solve1(&hard_puzzle1, 1);
    //piece_tree_testing();
}
