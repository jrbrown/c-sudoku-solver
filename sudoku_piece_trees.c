// 15/09/2020
// Jason Brown
// Sudoku solver sudoku_piece_tree.c


#include "sudoku_piece_trees.h"

#include <stdlib.h>
#include <stdio.h>


// Functions manipulating piece_trees
piece_tree_t * create_new_piece_tree_root(){
    piece_tree_t *pieceTree = NULL;

    // Dynamically assign memory
    pieceTree = (piece_tree_t *) malloc(sizeof(piece_tree_t));

    // Check malloc hasn't messed up
    if (pieceTree == NULL) {
        // Bad thing has happened
        exit(-1);

    } else{
        // Initialise all values and pointers
        for(int i=0; i<9; i++){
            pieceTree->values[i] = 0;
            pieceTree->branches[i] = NULL;
        }
        pieceTree->branches[9] = NULL;

        pieceTree->depth = 0;
        pieceTree->value = 0;
        pieceTree->parent = NULL;
    }

    return pieceTree;
}

void add_value_to_piece_tree(piece_tree_t * pieceTree, int value){
    // Check we're not at the end of the tree already
    if (pieceTree->depth == 9){
        exit(-2);
    }

    piece_tree_t *newPiece = NULL;

    // Dynamically assign memory
    newPiece = (piece_tree_t *) malloc(sizeof(piece_tree_t));

    // Check malloc hasn't messed up
    if (newPiece == NULL) {
        // Bad thing has happened
        exit(-1);

    } else{
        // Inherit all values and initialise pointers
        for(int i=0; i<9; i++){
            newPiece->values[i] = pieceTree->values[i];
            newPiece->branches[i] = NULL;
        }
        newPiece->branches[9] = NULL;

        // Increment depth
        newPiece->depth = pieceTree->depth + 1;

        // Add in new value
        newPiece->values[newPiece->depth - 1] = value;
        newPiece->value = value;

        // Link to parent
        pieceTree->branches[value] = newPiece;
        newPiece->parent = pieceTree;
    }
}

void add_value_to_piece_tree_if_valid(piece_tree_t * pieceTree, int value){

    // Check if value is already in tree
    for(int i=0; i<9; i++){
        if(pieceTree->values[i] == value){
            return;
        }
    }

    // Value is not in tree
    add_value_to_piece_tree(pieceTree, value);
}

void add_values_to_piece_tree_breadth(piece_tree_t * pieceTree, int values[9], void (*add_value_function)(piece_tree_t*, int)){
    // *add_value_function should point to add_value_to_piece_tree_if_valid or add_value_to_piece_tree

    for(int i=0; i<9; i++){
        if(values[i] != 0){
            add_value_function(pieceTree, values[i]);
        }
    }
}

void add_values_to_piece_tree_depth(piece_tree_t * pieceTree, int values[9], void (*add_value_function)(piece_tree_t*, int)){
    for(int i=0; i<9; i++){
        if(values[i] != 0){
            add_value_function(pieceTree, values[i]);
            pieceTree = pieceTree->branches[values[i]];
        }
    }
}

void add_values_to_all_branches_at_depth(piece_tree_t * pieceTree, int values[9], int depth){
    if(pieceTree->depth == depth){

        // At required depth so we should add in the values
        add_values_to_piece_tree_breadth(pieceTree, values, &add_value_to_piece_tree_if_valid);

    } else if(pieceTree->depth > depth){
        // Should not be here
        exit(-2);

    } else {
        // We are above required depth so we should iteratively recurse this function for our children
        for(int i=1; i<10; i++){
            if(pieceTree->branches[i] != NULL){
                add_values_to_all_branches_at_depth(pieceTree->branches[i], values, depth);
            }
        }
    }
}

piece_tree_t * generate_tree_from_possible_values_array(int possible_values[9][9]){
    piece_tree_t * root = create_new_piece_tree_root();

    for(int i=0; i<9; i++){
        add_values_to_all_branches_at_depth(root, possible_values[i], i);
    }

    return root;
}

void delete_branch(piece_tree_t * pieceTree){
    // We need to make sure all children get deleted too
    for(int i=1; i<10; i++){
        if(pieceTree->branches[i] != NULL){
            delete_branch(pieceTree->branches[i]);
        }
    }

    // No children so we can safely delete it
    // Check if we have a parent
    if(pieceTree->parent != NULL){
        piece_tree_t * parent = pieceTree->parent;
        int value = pieceTree->value;
        free(pieceTree);

        // Remove pointer on parent
        parent->branches[value] = NULL;
    } else {
        free(pieceTree);
    }
}

piece_tree_t * return_next_filled_piece(piece_tree_t * pieceTree){
    // Takes a piece tree node and returns the next node in the tree that is completely filled via a depth first search

    // Descend
    piece_tree_t * new_node = descend_tree(pieceTree);

    // Check if we have a new node and whether it's filled
    if(new_node != pieceTree){
        if(new_node->depth == 9){

            // New node is filed so we return it
            return new_node;

        } else {

            // New node is not filled so we recurse
            return return_next_filled_piece(new_node);
        }
    } else {

        // We are at the end of the tree
        return pieceTree;
    }
}

void compare_and_delete_non_compatible_trees(piece_tree_t * tree1, piece_tree_t * tree2, int tree1_positions[3], int tree2_positions[3]){
    // We'll depth recurse through tree1 and compare it to tree2 via another depth recurse
    // All non-matching nodes will be deleted

    // Iterate twice so we can remove from both trees
    for(int j=0; j<2; j++) {
        // Iterating through tree1
        piece_tree_t *tree1_previous_node = tree1;

        while (1) {
            // Get next filled piece
            piece_tree_t *tree1_filled = return_next_filled_piece(tree1_previous_node);

            // Check if we are at end of tree
            if (tree1_filled == tree1_previous_node) {
                break;
            }

            // Not at end so we'll now iterate through tree2
            piece_tree_t *tree2_previous_node = tree2;

            while (1) {
                // Get next filled piece
                piece_tree_t *tree2_filled = return_next_filled_piece(tree2_previous_node);

                // If we reach the end of our filled tree2 pieces then we haven't found a match yet so we delete the branch
                if (tree2_filled == tree2_previous_node) {

                    // Reset tree1 back to start
                    tree1_previous_node = tree1;

                    // Delete incompatible branch and move onto next tree1_filled
                    delete_branch(tree1_filled);
                    break;
                }

                // Check compatibility
                // First of all we collect the values we need to check
                int tree1_values[3] = {0};
                int tree2_values[3] = {0};
                for (int i = 0; i < 3; i++) {
                    tree1_values[i] = tree1_filled->values[tree1_positions[i]];
                    tree2_values[i] = tree2_filled->values[tree2_positions[i]];
                }

                // Compare values and if they match we can break out of this loop and check the next filled tree1
                if (tree1_values[0] == tree2_values[0] && tree1_values[1] == tree2_values[1] &&
                    tree1_values[2] == tree2_values[2]) {

                    // Update tree1 pointer
                    tree1_previous_node = tree1_filled;

                    break;
                }

                // Not at end of tree2_filled pieces and haven't found a match yet so we'll update the pointer to the next one
                tree2_previous_node = tree2_filled;
            }
        }

        // Swap things round on first iter and swap things back on second iter
        piece_tree_t * temp_pointer = tree1;
        tree1 = tree2;
        tree2 = temp_pointer;
        for(int i=0; i<3; i++){
            int temp_value = tree1_positions[i];
            tree1_positions[i] = tree2_positions[i];
            tree2_positions[i] = temp_value;
        }
    }
}

piece_tree_t * traverse_tree(piece_tree_t * pieceTree){

    // BROKEN! Doesn't take into account nodes at same level that have a different parent

    // Check if parent exists, if not we have no siblings
    if(pieceTree->parent != NULL) {
        // We have a parent so we might have higher value siblings so lets see if we can find any
        // Start iterator at value 1 after ours
        // We'll return the first one (ie lowest value) that we find
        for (int i = pieceTree->value + 1; i < 10; i++) {
            if (pieceTree->parent->branches[i] != NULL) {
                return pieceTree->parent->branches[i];
            }
        }

        // No higher value siblings so we check if we have any lower value siblings with children
        for (int i=1; i < pieceTree->value; i++) {
            if (pieceTree->parent->branches[i] != NULL) {

                // We have a lower value sibling so lets try and return their lowest value child
                for(int j=1; j<10; j++){
                    if(pieceTree->parent->branches[i]->branches[j] != NULL){
                        return pieceTree->parent->branches[i]->branches[j];
                    }
                }
            }
        }
    }

    // No higher value siblings or lower value siblings with children so now we return our lowest value child
    for(int i=1; i<10; i++){
        if(pieceTree->branches[i] != NULL){
            return pieceTree->branches[i];
        }
    }

    // No nephews, higher value siblings or children so we must be at end of tree, we'll return ourself
    return pieceTree;
}

piece_tree_t * descend_tree_internal(piece_tree_t * pieceTree, int min_child_value){
    int current_min_child_value = min_child_value;

    // Iterate through children and pick smallest greater than min_child_value
    for(int i=current_min_child_value; i<10; i++){
        if(pieceTree->branches[i] != NULL){
            return pieceTree->branches[i];
        }
    }

    // Check we have a parent, if no parent and no higher children we are or were at end of the tree
    // If we ended up here via recursion we went from the end of the tree back to the top
    // We need to return to the end of the tree and return that node
    if(pieceTree->parent == NULL){

        // Re-descend RHS of tree to get back to end
        for(int j=9; j>0; j--){
            if(pieceTree->branches[j] != NULL){
                pieceTree = pieceTree->branches[j];
                j = 10;
            }
        }

        // No children left therefore we are at end of tree and need to return self
        return pieceTree;
    }

    // No children and we have a parent so we'll recurse back to parent and update min child
    current_min_child_value = (pieceTree->value + 1);
    return descend_tree_internal(pieceTree->parent, current_min_child_value);
}

piece_tree_t * descend_tree(piece_tree_t * pieceTree){
    // Acts as a wrapper to reset min_child_value for when we are descending again
    return descend_tree_internal(pieceTree, 0);
}

// Debug functions
void print_piece(piece_tree_t * pieceTree){

    // Print values
    printf("\nValues: ");
    for(int i=0; i<9; i++){
        printf("%i", pieceTree->values[i]);
    }

    // Print depth
    printf(" Depth: %i", pieceTree->depth);

    // Print branches
    printf(" Branches: ");
    for(int i=1; i<10; i++){
        if(pieceTree->branches[i] != NULL){
            printf("1");
        } else {
            printf("0");
        }
    }
}

void print_piece_depth_recurse(piece_tree_t * pieceTree){
    print_piece(pieceTree);

    // Recursively print children
    for(int i=1; i<10; i++){
        if(pieceTree->branches[i] != NULL){
            print_piece_depth_recurse(pieceTree->branches[i]);
        }
    }
}

void print_piece_tree_depth_first(piece_tree_t * pieceTree){
    printf("\nPiece Tree");
    print_piece_depth_recurse(pieceTree);
    printf("\n");
}

void print_piece_tree_depth_first2(piece_tree_t * pieceTree){
    printf("\nPiece Tree");

    int finished = 0;
    while (!finished){
        print_piece(pieceTree);
        piece_tree_t *old_tree = pieceTree;
        pieceTree = descend_tree(pieceTree);
        finished = (old_tree == pieceTree);
    }

    printf("\n");
}

void print_piece_tree_breadth_first(piece_tree_t * pieceTree){
    printf("\nPiece Tree");

    int finished = 0;
    while (!finished){
        print_piece(pieceTree);
        piece_tree_t *old_tree = pieceTree;
        pieceTree = traverse_tree(pieceTree);
        finished = (old_tree == pieceTree);
    }

    printf("\n");
}

void print_piece_depth_recurse_if_filled(piece_tree_t * pieceTree){
    if(pieceTree->depth == 9){
        print_piece(pieceTree);
    }

    // Recursively print children
    for(int i=1; i<10; i++){
        if(pieceTree->branches[i] != NULL){
            print_piece_depth_recurse_if_filled(pieceTree->branches[i]);
        }
    }
}

void print_filled_pieces(piece_tree_t * pieceTree){
    printf("\n");
    print_piece_depth_recurse_if_filled(pieceTree);
    printf("\n");
}

// Main function to test things
void piece_tree_testing(){

    int possible_values1[9][9] = {
            {1},
            {2},
            {3, 9},
            {4},
            {5},
            {6},
            {7, 8, 9},
            {7, 8, 9},
            {3, 9}
    };

    piece_tree_t * test_piece1 = generate_tree_from_possible_values_array(possible_values1);
    print_filled_pieces(test_piece1);
    //print_piece_tree_depth_first(test_piece1);
    //print_piece_tree_depth_first2(test_piece1);

    int possible_values2[9][9] = {
            {7, 8, 9},
            {7, 8, 9},
            {3, 4},
            {1},
            {2},
            {9},
            {3, 4, 5},
            {4, 5},
            {6}
    };

    piece_tree_t * test_piece2 = generate_tree_from_possible_values_array(possible_values2);
    print_filled_pieces(test_piece2);

    int test_piece1_positions[3] = {6, 7, 8};
    int test_piece2_positions[3] = {0, 1, 2};
    compare_and_delete_non_compatible_trees(test_piece1, test_piece2, test_piece1_positions, test_piece2_positions);

    print_filled_pieces(test_piece1);
    print_filled_pieces(test_piece2);
}
